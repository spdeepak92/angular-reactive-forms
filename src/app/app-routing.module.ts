import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SimpleComponent } from './simple/simple.component';
import { NestedFormComponent } from './nested-form/nested-form.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';

import { MainRootComponent } from './main-root/main-root.component';
import { Child1Component } from './child1/child1.component';
import { Child2Component } from './child2/child2.component';

const routes: Routes = [
  { path: '', redirectTo: '/simple', pathMatch: 'full'},
  { path: 'simple', component: SimpleComponent},
  { path: 'nested', component: NestedFormComponent},
  { path: 'dynamic', component: DynamicFormComponent},
  {
    path: 'root',
    component: MainRootComponent,
    children: [
      { path: 'child1', component: Child1Component },
      { path: 'child2', component: Child2Component },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
                                  SimpleComponent,
                                  NestedFormComponent,
                                  DynamicFormComponent,
                                  MainRootComponent,
                                  Child1Component,
                                  Child2Component
                                ];
