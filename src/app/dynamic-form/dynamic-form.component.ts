import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  formStatus = 'error';
  result: any;

  constructor(private _form: FormBuilder) { }

  code = [91, 92, 93, 94, 95];

  ngOnInit() {
    this.loginForm = this._form.group({
      email: ['', [Validators.required, Validators.email]],
      phones: this._form.array([]),
    });
  }

  get phoneForms() {
    return this.loginForm.get('phones') as FormArray;
  }

  addPhoneForm() {
    const addPhone = this._form.group({
      code: ['', Validators.required],
      contact: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(10)]]
    });
    this.phoneForms.push(addPhone);
  }

  deletePhoneForm(i) {
    this.phoneForms.removeAt(i);
  }

  login() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      this.formStatus = 'error';
      this.result = '';
      return;
    }

    if (this.loginForm.valid) {
      this.formStatus = 'success';
    }

    this.result = JSON.stringify(this.loginForm.value);
  }

}
