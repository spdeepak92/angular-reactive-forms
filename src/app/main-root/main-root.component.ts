import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-main-root',
  template: `
    <div>
      <div class="example-button-row">
        <button mat-raised-button color="primary" (click)="child1()">TO Child 1</button>
        <button mat-raised-button color="primary" (click)="child2()">TO Child 2</button>
      </div>

      <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class MainRootComponent implements OnInit {

  constructor(private _activeRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
  }

  child1() {
    this._router.navigate(['child1'], {relativeTo: this._activeRoute});
  }

  child2() {
    this._router.navigate(['child2'], {relativeTo: this._activeRoute});
  }
}
