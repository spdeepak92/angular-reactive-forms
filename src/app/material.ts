import { NgModule } from '@angular/core';

import {
    MatToolbarModule,
    MatInputModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatButtonModule,
    MatSelectModule
} from '@angular/material';

@NgModule({
    imports: [  MatToolbarModule,
                MatCheckboxModule,
                MatSidenavModule,
                MatInputModule,
                MatIconModule,
                MatListModule,
                MatExpansionModule,
                MatButtonModule,
                MatSelectModule,
            ],

    exports: [  MatToolbarModule,
                MatCheckboxModule,
                MatSidenavModule,
                MatInputModule,
                MatIconModule,
                MatListModule,
                MatExpansionModule,
                MatButtonModule,
                MatSelectModule,
            ],

})
export class MaterialModule { }
