import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-nested-form',
  templateUrl: './nested-form.component.html',
  styleUrls: ['./nested-form.component.css']
})

export class NestedFormComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  formStatus = 'error';
  result: any;

  constructor(private _form: FormBuilder) { }

  code = [91, 92, 93, 94, 95];

  ngOnInit() {

    const phone = this._form.group({
      code: ['', Validators.required],
      contact: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]]
    });

    this.loginForm = this._form.group({
      email: ['', [Validators.required, Validators.email]],
      homePhone: phone,
      OfficePhone: phone
    });

  }

  login() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      this.formStatus = 'error';
      this.result = '';
      return;
    }

    if (this.loginForm.valid) {
      this.formStatus = 'success';
    }

    this.result = JSON.stringify(this.loginForm.value);
  }
}
