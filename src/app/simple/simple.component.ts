import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-simple',
  templateUrl: './simple.component.html',
  styleUrls: ['./simple.component.css']
})

export class SimpleComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  formStatus = 'error';
  result: any;

  constructor(private _form: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this._form.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  login() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      this.formStatus = 'error';
      this.result = '';
      return;
    }

    if (this.loginForm.valid) {
      this.formStatus = 'success';
    }

    this.result = JSON.stringify(this.loginForm.value);

  }

}
